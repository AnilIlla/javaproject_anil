package com.allstate.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories(basePackages = "com.allstate.boot.data")
public class AllstatePaymentSystemApplication {
	public static void main(String[] args) {
		SpringApplication.run(AllstatePaymentSystemApplication.class, args);
	}
}
