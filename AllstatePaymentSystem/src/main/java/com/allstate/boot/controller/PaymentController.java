package com.allstate.boot.controller;

import com.allstate.boot.exceptions.PaymentNotFoundException;
import com.allstate.boot.entities.Payment;
import com.allstate.boot.services.PaymentBS;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import org.slf4j.Logger;

@RestController
@RequestMapping("/payment/gateway")
public class PaymentController {

    Logger logger = LoggerFactory.getLogger(PaymentController.class);

    @Autowired
    private PaymentBS paymentService;

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String getStatus() {
        logger.info("starting method status ");
        return "connected";
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    public int getRowCount() {
        logger.info("starting method rowcount ");
        int count = paymentService.rowcount();
        logger.info("no of documents in collection "+count);
        return count;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    public int addPayment(@RequestBody Payment payment) {
        logger.info("starting method addPayment ");
        int myPayment = paymentService.save(payment);
        logger.info("payment details added : "+myPayment);
        return myPayment;
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
    public Payment findById(@PathVariable int id) {
        logger.info("starting method findById with id " + id);
        Payment myPayement = paymentService.findById(id);
        logger.info("payment details fetched :" + myPayement);
        return myPayement;
    }

    @RequestMapping(value = "/type/{type}", method = RequestMethod.GET)
    public List<Payment> findByType(@PathVariable String type) {
        logger.info("starting method findById with type "+type);
        List<Payment> myPayements = paymentService.findByType(type);
        logger.info("payment details fetched :"+myPayements);
        return myPayements;
    }
}
