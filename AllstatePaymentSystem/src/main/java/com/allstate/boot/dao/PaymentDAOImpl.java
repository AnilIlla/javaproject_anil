package com.allstate.boot.dao;

import com.allstate.boot.data.PaymentMongoRepository;
import com.allstate.boot.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PaymentDAOImpl implements  PaymentDAO {

    @Autowired
    private PaymentMongoRepository myPaymentMongoRepository;

    public int rowcount() {
        return (int)myPaymentMongoRepository.count();
    }

    public Payment findById(int id) {
        return myPaymentMongoRepository.findById(id);
    }

    public List<Payment> findByType(String type) {
        return myPaymentMongoRepository.findByType(type);
    }

    public Payment save(Payment payment) {
        return myPaymentMongoRepository.save(payment);
    }
}
