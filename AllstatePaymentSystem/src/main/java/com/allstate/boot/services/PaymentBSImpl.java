package com.allstate.boot.services;

import com.allstate.boot.dao.PaymentDAO;
import com.allstate.boot.entities.Payment;
import com.allstate.boot.exceptions.InvalidPaymentRequestException;
import com.allstate.boot.exceptions.PaymentNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.List;
@Service
public class PaymentBSImpl implements PaymentBS {

    @Autowired
    private PaymentDAO myPaymentDAO;

    public int rowcount() {
        return myPaymentDAO.rowcount();
    }

    public Payment findById(int id) {
        Payment myPayment = null;
        if (id > 0) {
            myPayment = myPaymentDAO.findById(id);;
            if (myPayment == null) {
                throw new PaymentNotFoundException("No payment details found for id -: "+id);
            }
        } else {
            throw new InvalidPaymentRequestException("id of payment cannot be lessthan 0");
        }
        return myPayment;
    }

    public List<Payment> findByType(String type) {
        List<Payment> myPayments = null;
        if (type != null || !type.trim().equals("")) {
            myPayments = myPaymentDAO.findByType(type);
            if (myPayments==null || myPayments.size()==0) {
                throw new PaymentNotFoundException("No payment details found for type -: "+type);
            }
        } else {
            throw new InvalidPaymentRequestException("type of payment cannot be null or empty");
        }
        return myPayments;
    }

    public int save(Payment payment) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            String formattedDate = formatter.format(payment.getPaymentdate());
            payment.setPaymentdate(formatter.parse(formattedDate));
            Payment savedPayment = myPaymentDAO.save(payment);
            if (savedPayment != null && savedPayment.getId()== payment.getId())
                return 1;
            else
                return 0;
        } catch (Exception exp) {
            return 0;
        }
    }
}
