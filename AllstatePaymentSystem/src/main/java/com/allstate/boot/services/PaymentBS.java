package com.allstate.boot.services;

import com.allstate.boot.entities.Payment;
import org.springframework.stereotype.Service;

import java.util.List;

public interface PaymentBS {

    public int rowcount();

    public Payment findById(int id);

    public List<Payment> findByType(String type);

    public int save(Payment payment);

}
