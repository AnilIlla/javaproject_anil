package com.allstate.boot.controller;

import ch.qos.logback.core.net.SyslogOutputStream;
import com.allstate.boot.AllstatePaymentSystemApplication;
import com.allstate.boot.entities.Payment;
import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AllstatePaymentSystemApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PaymentControllerTest {

    @LocalServerPort
    private int localServerPort;

    TestRestTemplate restTemplate = new TestRestTemplate();

    HttpHeaders headers = new HttpHeaders();

    private String createURLWithPort(String uri) {
        return "http://localhost:" + localServerPort + uri;
    }

    @Test
    public void test_findById_2() throws JSONException {

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/payment/gateway/id/"+2),
                HttpMethod.GET, entity, String.class);
        String expected = "{\"id\":2,\"paymentdate\":\"2020-11-09T11:27:06.369+00:00\",\"type\":\"upi\",\"amount\":342.35,\"custid\":12345}";
        JSONAssert.assertEquals(expected, response.getBody(), false);
    }

    @Test
    public void test_findByType_upi() throws JSONException {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/payment/gateway/type/upi"),
                HttpMethod.GET, entity, String.class);
        String expected = "[{\"id\":2,\"paymentdate\":\"2020-11-09T11:27:06.369+00:00\",\"type\":\"upi\",\"amount\":342.35,\"custid\":12345},{\"id\":3,\"paymentdate\":\"2020-11-09T11:45:59.166+00:00\",\"type\":\"upi\",\"amount\":342.35,\"custid\":12345},{\"id\":4,\"paymentdate\":\"2020-11-09T12:24:40.453+00:00\",\"type\":\"upi\",\"amount\":342.35,\"custid\":12345},{\"id\":1,\"paymentdate\":\"2020-11-10T06:32:14.227+00:00\",\"type\":\"upi\",\"amount\":342.35,\"custid\":12345},{\"id\":5,\"paymentdate\":\"2020-11-09T13:00:23.402+00:00\",\"type\":\"upi\",\"amount\":342.35,\"custid\":12345},{\"id\":6,\"paymentdate\":\"2020-11-09T18:35:11.000+00:00\",\"type\":\"upi\",\"amount\":342.35,\"custid\":12345},{\"id\":8,\"paymentdate\":\"2020-11-10T00:00:00.000+00:00\",\"type\":\"upi\",\"amount\":15.26,\"custid\":234},{\"id\":9,\"paymentdate\":\"2020-11-10T00:00:00.000+00:00\",\"type\":\"upi\",\"amount\":15.26,\"custid\":234}]";
        JSONAssert.assertEquals(expected, response.getBody(), false);
    }

    @Test
    public void test_addPayment() {
        Date todayDate = new Date();
        Payment myPayment = new Payment(7, todayDate, "upi", 342.35, 12345);
        HttpEntity<Payment> entity = new HttpEntity<Payment>(myPayment, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/payment/gateway/save"),
                HttpMethod.POST, entity, String.class);
        assertEquals("1",response.getBody());

    }

    @Test
    public void test_rowCount() throws JSONException {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/payment/gateway/count"),
                HttpMethod.GET, entity, String.class);
        JSONAssert.assertEquals("9", response.getBody(), false);
    }

    @Test
    public void status() throws JSONException {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/payment/gateway/status"),
                HttpMethod.GET, entity, String.class);
        assertEquals("connected", response.getBody());
    }
}
