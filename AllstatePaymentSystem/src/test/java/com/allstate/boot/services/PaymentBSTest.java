package com.allstate.boot.services;

import com.allstate.boot.services.PaymentBS;
import com.allstate.config.PaymentMongoConfig;
import com.allstate.boot.entities.Payment;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = PaymentMongoConfig.class, loader = AnnotationConfigContextLoader.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PaymentBSTest  {

    @Autowired
    PaymentBS myPaymentBS;

    @Test
    public void test_addPaymentDetails()  {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date todayDate = formatter.parse("09/11/2020 18:35:11");
            Payment myPayment = new Payment(6, todayDate, "upi", 342.35, 12345);
            int saved = myPaymentBS.save(myPayment);
            assertEquals(1,saved);
        } catch (ParseException exception) {
            assertFalse(false);
        }
    }
    @Test
    public void test_rowCount() {
        assertEquals(5,myPaymentBS.rowcount());
    }
    @Test
    public void test_findById() {
        assertEquals(342.35,myPaymentBS.findById(3).getAmount());
    }
    @Test
    public void test_negitive_findById() {
        assertEquals(null,myPaymentBS.findById(-7));
    }
    @Test
    public void test_findByType() {
        myPaymentBS.findByType("upi").forEach(payment -> assertEquals(342.35,payment.getAmount()));
    }
    @Test
    public void test_negitive_findByType() {
        assertEquals(null,myPaymentBS.findByType(null));
    }
}

