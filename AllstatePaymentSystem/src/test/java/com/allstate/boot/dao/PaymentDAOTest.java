package com.allstate.boot.dao;

import com.allstate.boot.dao.PaymentDAO;
import com.allstate.config.PaymentMongoConfig;
import com.allstate.boot.entities.Payment;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = PaymentMongoConfig.class, loader = AnnotationConfigContextLoader.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PaymentDAOTest {

    @Autowired
    PaymentDAO myPaymentDAO;

    @Test
    public void test_addPaymentDetails() {
        Date todayDate = new Date();
        Payment myPayment = new Payment(1, todayDate, "upi", 342.35, 12345);
        Payment savedPayment = myPaymentDAO.save(myPayment);
        assertEquals(342.35,savedPayment.getAmount());
    }
    @Test
    public void test_rowCount() {
        assertEquals(4,myPaymentDAO.rowcount());
    }
    @Test
    public void test_findById() {
        assertEquals(342.35,myPaymentDAO.findById(3).getAmount());
    }
    @Test
    public void test_findByType() {
        myPaymentDAO.findByType("upi").forEach(payment -> assertEquals(342.35,payment.getAmount()));
    }
}
